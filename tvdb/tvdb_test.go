package tvdb

import "testing"

import (
	"fmt"
	"net/http"
	"net/http/httptest"
)

const (
	ID          = "123456"
	NAME        = "serieName"
	DESCRIPTION = "some description"
	SEARCH_PATH = "/GetSeries.php?seriesname=" + NAME
	SEARCH_RESP = `<Data>
	                 <Series>
					   <seriesid>` + ID + `</seriesid>
					   <SeriesName>` + NAME + `</SeriesName>
					   <Overview>` + DESCRIPTION + `</Overview>
	                 </Series>
	               </Data>`
)

func TestSearch(t *testing.T) {
	ts := NewServer(t, map[string]string{
		SEARCH_PATH: SEARCH_RESP,
	})
	defer ts.Close()
	tvdb := NewTvdb("")
	tvdb.url = ts.URL

	series, err := tvdb.SearchName(NAME)
	if err != nil {
		t.Fatalf("There was an error searching: %v", err)
	}
	if len(series) != 1 {
		t.Fatalf("Number of series unexpected: %d", len(series))
	}
	if series[0].Id != ID {
		t.Error("Id don't match: %s <=> %s", series[0].Id, ID)
	}
	if series[0].Title() != NAME {
		t.Error("Title don't match: %s <=> %s", series[0].Title(), NAME)
	}
	if series[0].Description() != DESCRIPTION {
		t.Error("Description don't match: %s <=> %s", series[0].Description(), DESCRIPTION)
	}
}

func NewServer(t *testing.T, responses map[string]string) *httptest.Server {
	return httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		response, ok := responses[r.URL.String()]

		if ok {
			fmt.Fprint(w, response)
		} else {
			t.Fatalf("Not valid url: %s", r.URL.String())
		}
	}))
}
