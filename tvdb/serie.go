package tvdb

type Serie struct {
	Id         string `xml:"seriesid"`
	SeriesName string
	Overview   string
}

func (s Serie) Title() string {
	return s.SeriesName
}

func (s Serie) Description() string {
	return s.Overview
}

func (s Serie) Seasons() ([]Season, error) {
	return nil, nil
}

type Season struct {
	Id     string
	Number int
	//Episodes
}
