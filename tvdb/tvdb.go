package tvdb

import (
	"encoding/xml"
	"io/ioutil"
	"net/http"
)

const (
	URL = "http://thetvdb.com/api/"
)

type Tvdb struct {
	key string
	url string
}

func NewTvdb(key string) *Tvdb {
	return &Tvdb{key, URL}
}

func (t Tvdb) SearchName(name string) ([]Serie, error) {
	resp, err := http.Get(t.url + "/GetSeries.php?seriesname=" + name)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var series struct {
		Series []Serie
	}
	xml.Unmarshal(body, &series)
	return series.Series, nil
}
