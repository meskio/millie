package main

type Serie interface {
	Title() string
	Description() string
	Seasons() ([]Season, error)
}

type Season interface {
	Number() int
	Episodes() ([]Episode, error)
}

type Episode interface {
	Title() string
	Number() int
	Description() string
}
